/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file monomials.cpp
 * @brief Implementation file for Bases/monomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/monomials.hpp"

namespace Alea {

Array3d Monomials::recur_coeffs( int p ) const
{
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        rc( n, 0 ) = 0;
        rc( n, 1 ) = 1;
        rc( n, 2 ) = 0;
    }
    return rc;
}

bool Monomials::is_monic() const
{
    return true;
}

bool Monomials::is_normalised() const
{
    return false;
}

bool Monomials::is_orthogonal() const
{
    return false;
}

} // namespace Alea
