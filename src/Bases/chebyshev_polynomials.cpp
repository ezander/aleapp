/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file chebyshev_polynomials.cpp
 * @brief Implementation file for Bases/chebyshev_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/chebyshev_polynomials.hpp"

namespace Alea {

Array3d ChebyshevTPolynomials::recur_coeffs( int p ) const
{
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        rc( n, 0 ) = 0;
        rc( n, 1 ) = 2 - ( n == 0 );
        rc( n, 2 ) = 1;
    }
    return rc;
}

bool ChebyshevTPolynomials::is_monic() const
{
    return false;
}

bool ChebyshevTPolynomials::is_normalised() const
{
    return false;
}

bool ChebyshevTPolynomials::is_orthogonal() const
{
    return true;
}

Array3d ChebyshevUPolynomials::recur_coeffs( int p ) const
{
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        rc( n, 0 ) = 0;
        rc( n, 1 ) = 2;
        rc( n, 2 ) = 1;
    }
    return rc;
}

bool ChebyshevUPolynomials::is_monic() const
{
    return false;
}

bool ChebyshevUPolynomials::is_normalised() const
{
    return true;
}

bool ChebyshevUPolynomials::is_orthogonal() const
{
    return true;
}

} // namespace Alea
