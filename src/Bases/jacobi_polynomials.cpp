/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file jacobi_polynomials.cpp
 * @brief Implementation file for Bases/jacobi_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/jacobi_polynomials.hpp"

namespace Alea {

JacobiPolynomials::JacobiPolynomials( double alpha, double beta )
    : m_alpha( alpha ), m_beta( beta ) {}

Array3d JacobiPolynomials::recur_coeffs( int p ) const
{
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        double an = 1.0 * ( m_alpha + m_beta ) * ( m_alpha - m_beta )
                * ( 2 * n + m_alpha + m_beta + 1 )
                * ( 2 * n + m_alpha + m_beta );
        double bn = 1.0 * ( 2 * n + m_alpha + m_beta + 1 )
                * ( 2 * n + m_alpha + m_beta + 2 );
        double cn = 2.0 * ( n + m_alpha ) * ( n + m_beta )
                * ( 2 * n + m_alpha + m_beta + 2 );
        double dn = 2.0 * ( n + 1 ) * ( n + m_alpha + m_beta + 1 )
                * ( 2 * n + m_alpha + m_beta );

        rc( n, 0 ) = an / dn;
        rc( n, 1 ) = bn / dn;
        rc( n, 2 ) = cn / dn;
    }
    return rc;
}
bool JacobiPolynomials::is_monic() const
{
    return false;
}

bool JacobiPolynomials::is_normalised() const
{
    return false;
}

bool JacobiPolynomials::is_orthogonal() const
{
    return true;
}

} // namespace Alea
