/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file hermite_polynomials.cpp
 * @brief Implementation file for Bases/hermite_polynomials.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/Bases/hermite_polynomials.hpp"

namespace Alea {

HermitePolynomials::HermitePolynomials ( bool probabilist )
    : m_probabilist( probabilist ) {}

Array3d HermitePolynomials::recur_coeffs( int p ) const
{
    int factor = m_probabilist ? 1 : 2;
    Array3d rc( p, 3 );
    for( int n = 0; n < p; n++ ) {
        rc( n, 0 ) = 0;
        rc( n, 1 ) = factor * 1;
        rc( n, 2 ) = factor * n;
    }
    return rc;
}

Array1d cumprod( Eigen::Ref<const Array1d> v, double p0 = 1)
{
    int n = static_cast<int>( v.size() );
    Array1d w(n + 1);
    w(0) = p0;
    for( int i = 0; i < n; i++ ) {
        w(i+1) = w(i) * v(i);
    }
    return w;
}
Array1d HermitePolynomials::norm( int max_degree ) const
{
    return cumprod( Array1d::LinSpaced( max_degree,  1,  max_degree ) ).sqrt();
}


bool HermitePolynomials::is_monic() const
{
    return m_probabilist;
}

bool HermitePolynomials::is_normalised() const
{
    return false;
}

bool HermitePolynomials::is_orthogonal() const
{
    return true;
}

} // namespace Alea
