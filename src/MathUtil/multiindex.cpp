/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file multiindex.cpp
 * @brief Implementation file for MathUtil/multiindex.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/multiindex.hpp"
#include "Alea/src/MathUtil/specfun.hpp"

namespace Alea {

MultiindexSet::MultiindexSet()
    : values() {}

const MultiindexArray& MultiindexSet::get_values() const
{
    return values;
}

MultiindexArray& MultiindexSet::get_values()
{
    return values;
}

void MultiindexSet::set_values( const MultiindexArray& new_values )
{
    values = new_values;
}

void fillmat( MultiindexArray& v, const int m, const int p, Array1i& r )
{
    if( m == 0 ) {
        r.setOnes( p + 1 );
        return;
    }

    fillmat( v, m - 1, p, r );
    long int s0 = v.rows() - r( p );
    long int s = s0;
    for( int pc = p; pc >= 0; pc-- ) {
        int sz = r( pc );
        if( pc != p ) {
            v.block( s, 0, sz, m - 1 ) = v.block( s0, 0, sz, m - 1 );
        }

        v.block( s, m - 1, sz,
                1 ) = pc - v.block( s0, 0, sz, m - 1 ).rowwise().sum();
        if( pc > 0 )
            s -= r( pc - 1 );
    }
    for( int pc = 0; pc < p; pc++ ) {
        r( pc + 1 ) += r( pc );
    }
}

MultiindexSet MultiindexSet::create_complete_set( int m, int p )
{
    // TODO: need to check cast in the following line
    int M = static_cast<int>( binomial( m + p, p ) );

    MultiindexArray values;
    values.setZero( M, m );

    Array1i r;
    fillmat( values, m, p, r );

    MultiindexSet set;
    set.set_values( values );
    return set;
}

} // namespace Alea
