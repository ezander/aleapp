/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file specfun.cpp
 * @brief Implementation file for MathUtil/specfun.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/specfun.hpp"

#include <gsl/gsl_sf_gamma.h>

namespace Alea {

double factorial( int n )
{
    return gsl_sf_fact( n );
}

double binomial( int n, int k )
{
    return gsl_sf_choose( n, k );
}

} // namespace Alea
