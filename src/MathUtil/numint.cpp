/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file numint.cpp
 * @brief Implementation file for MathUtil/numint.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/numint.hpp"

#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

namespace Alea {

double UnaryFunc::do_eval( double x, void* self_ptr )
{
    UnaryFunc* self = static_cast<UnaryFunc*>( self_ptr );
    return self->eval( x );
}

UnaryFunc::~UnaryFunc() {}

struct Quadrature1d::workspace
    : gsl_integration_workspace {};

Quadrature1d::~Quadrature1d()
{
    gsl_integration_workspace_free( m_ws );
}
Quadrature1d::Quadrature1d()
    : m_last_error( 0.0 ),
    m_last_status( 0 ),
    m_ws_size( 10 ),
    m_ws( 0 )
{
    m_ws = ( workspace* )gsl_integration_workspace_alloc( m_ws_size );
}
int Quadrature1d::intervals()
{
    return static_cast<int>( m_ws->size );
}

void handler( const char * reason,
        const char * file,
        int line,
        int gsl_errno )
{
    printf( "GSL_ERROR: %s %s %d %d\n", reason, file, line, gsl_errno );
}

static gsl_function as_gsl_function( const UnaryFunc& func )
{
    gsl_function F;
    F.function = UnaryFunc::do_eval;
    F.params = const_cast<void*>( static_cast<const void*>( &func ) );
    return F;
}

double Quadrature1d::quad1d( const UnaryFunc& df, double a, double b )
{
    double epsabs = 1e-4;
    double epsrel = 1e-4;

    gsl_set_error_handler( handler );

    double result;

    gsl_function F = as_gsl_function( df );

    if( isinf( a ) )
        if( isinf( b ) )
            m_last_status = gsl_integration_qagi( &F,
                    epsabs, epsrel, m_ws_size, m_ws, &result, &m_last_error );
        else
            m_last_status = gsl_integration_qagil( &F, b,
                    epsabs, epsrel, m_ws_size, m_ws, &result, &m_last_error );
    else if( isinf( b ) )
        m_last_status = gsl_integration_qagiu( &F, a,
                epsabs, epsrel, m_ws_size, m_ws, &result, &m_last_error );
    else
        m_last_status = gsl_integration_qags( &F, a, b,
                epsabs, epsrel, m_ws_size, m_ws, &result, &m_last_error );

    return result;
}
double Quadrature1d::get_last_error() const
{
    return m_last_error;
}

int Quadrature1d::get_last_status() const
{
    return m_last_status;
}

} // namespace Alea
