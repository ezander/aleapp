/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file gpc.cpp
 * @brief Implementation file for GPC/gpc.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/GPC/gpc.hpp"
#include <Alea/Polynomials>
#include <Alea/Distributions>

namespace Alea {

// GPCPair methods
GPCPair::GPCPair( Ref<const ContinuousDistribution> dist,
        Ref<const PolynomialSystem> sys )
    : m_dist( dist ), m_sys( sys ), m_id( ++s_id ) {}

Ref<const ContinuousDistribution> GPCPair::get_dist() const
{
    return m_dist;
}

Ref<const PolynomialSystem> GPCPair::get_system() const
{
    return m_sys;
}

int GPCPair::s_id = 0;

// GPCGerm methods
void GPCGerm::add_pair( GPCPair pair )
{
    m_pairs.push_back( pair );
}

// GPCRegistry methods
void GPCRegistry::add_pair( char c, const GPCPair& pair )
{
    assert( !m_char_to_gpcpair.count( c ) && "GPC key must be unique" );
    m_char_to_gpcpair.insert( std::make_pair( c, pair ) );
}

GPCPair GPCRegistry::get_pair( char c ) const
{
    assert( m_char_to_gpcpair.count( c ) && "GPC key must exist" );
    std::map<char, GPCPair>::const_iterator it = m_char_to_gpcpair.find( c );
    return it->second;
}

GPCGerm GPCRegistry::create_germ( const std::string& str ) const
{
    GPCGerm germ;
    for( unsigned i = 0; i < str.length(); i++ ) {
        germ.add_pair( this->get_pair( str[i] ) );

    }
    return germ;
}

GPCRegistry create_default_registry()
{
    GPCRegistry registry;

    Ref<ContinuousDistribution> uniform( new UniformDistribution( -1, 1 ) );
    Ref<PolynomialSystem> legendre( new LegendrePolynomials() );
    registry.add_pair( 'P', GPCPair( uniform, legendre ) );

    Ref<ContinuousDistribution> normal( new NormalDistribution( 0, 1 ) );
    Ref<PolynomialSystem> hermite( new HermitePolynomials() );
    registry.add_pair( 'H', GPCPair( normal, hermite ) );

    Ref<ContinuousDistribution> exponential( new ExponentialDistribution( 1 ) );
    Ref<PolynomialSystem> laguerre( new LaguerrePolynomials( 0 ) );
    registry.add_pair( 'L', GPCPair( exponential, laguerre ) );

    return registry;
}

GPCRegistry GPCRegistry::get_default()
{
    if( !s_default_reg.m_char_to_gpcpair.size() )
        s_default_reg = create_default_registry();
    return s_default_reg;
}
GPCRegistry GPCRegistry::s_default_reg;

} // namespace Alea
