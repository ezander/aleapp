macro(add_cxx_compile_option OPTION OPTION_NAME)
  include(CheckCXXCompilerFlag)

  set(VARNAME "CXX_SUPPORTS_ARG_${OPTION_NAME}")

  set (EXTRA_ARGS ${ARGN})
  list(LENGTH EXTRA_ARGS NUM_EXTRA_ARGS)

  if( ${NUM_EXTRA_ARGS} GREATER 0 )
    list(GET EXTRA_ARGS 0 RESET)
    if( ${RESET} MATCHES "TRUE|RESET|YES" )
      unset(${VARNAME} CACHE)
    endif()
  endif()
  
  check_cxx_compiler_flag(${OPTION} "${VARNAME}")

  if(${VARNAME})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OPTION}")
  endif()

endmacro(add_cxx_compile_option)

