#!/usr/bin/env python

from __future__ import print_function
import sys, os
from string import Template, maketrans

def subst(str, **kwds):
    substs=globals().copy()
    substs.update(kwds)
    return Template(str).substitute(substs)


relpath = sys.argv[1]
basename = sys.argv[2]
basepath = os.getcwd()


project_desc = "Alea++, a C++ library for Uncertainty Quantification."
year = "2015"
author = "Elmar Zander"
email = "<e.zander@tu-braunschweig.de>"
affiliation = "Inst. of Scientific Computing, TU Braunschweig"


def license_template():
    return """\
/*
 * This file is part of $project_desc
 *
 * Copyright (C) $year $author $email
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */"""


def doxygen_template():
    return """\
/**
 * @file $filename
 * @brief $filetype
 * @author $author, $affiliation
 */"""

def header_template():
    return """\
$license

$doxygen

$guard_begin

namespace Alea {
$old_content
} // namespace Alea

$guard_end
"""

def impl_template():
    return """\
$license

$doxygen

#include <Alea/Common>
#include <Alea/Types>
#include "$include_name"

namespace Alea {

$old_content

} // namespace Alea
"""


def test_template():
    return """\
$license

$doxygen

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "$include_name"

using namespace Alea;

#define BASE_TAGS "$base_tags"

$old_content
"""

impl_name = subst("$basename.cpp")
header_name = subst("$basename.hpp")
test_name = subst("test_$basename.cpp")

impl_path = subst("src/$relpath/$impl_name")
header_path = subst("include/Alea/src/$relpath/$header_name")
test_path = subst("test/src/$relpath/$test_name")

include_name = header_path.replace("include/", "")

guard_string = subst("alea_$relpath/$header_name").upper().translate(maketrans("./","__"))
guard_begin=subst("#ifndef $guard_string\n#define $guard_string")
guard_end=subst("#endif // $guard_string")

base_tags=subst("[$relpath_lower][$basename]", relpath_lower=relpath.lower())

header_filetype=subst("Header file")
impl_filetype=subst("Implementation file for $relpath/$header_name")
test_filetype=subst("Unit tests for $relpath/$header_name")

from query import query_yes_no

def format_file(template, **substs):
    global license, doxygen, fullpath, old_content
    license=subst(license_template())
    doxygen=subst(doxygen_template(), **substs)
    fullpath = subst("$basepath/$path", **substs)

    does_exist = os.path.isfile(fullpath)
    if does_exist:
        question = "File %s already exists. Overwrite?" % fullpath
    else:
        question = "File %s does not exist. Create?" % fullpath
    if query_yes_no(question) != "yes":
        return

    if does_exist:
        with open(fullpath, 'r') as f:
            old_content = f.read()
    else:
        old_content = ""

    new_content = subst(template)

    directory = os.path.dirname(fullpath)
    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(fullpath, 'w') as f:
            f.write(new_content)


format_file(header_template(),
            filename=header_name, filetype=header_filetype, path=header_path)

format_file(impl_template(),
            filename=impl_name, filetype=impl_filetype, path=impl_path)

format_file(test_template(),
            filename=test_name, filetype=test_filetype, path=test_path)

