/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file gpc.hpp
 * @brief Header file for the GPC class (and related classes)
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_GPC_GPC_HPP
#define ALEA_GPC_GPC_HPP

namespace Alea {

class ContinuousDistribution;
class PolynomialSystem;
class MultiindexSet;

class GPCPair {
public:
    GPCPair( Ref<const ContinuousDistribution> dist,
            Ref<const PolynomialSystem> sys );
    Ref<const ContinuousDistribution> get_dist() const;
    Ref<const PolynomialSystem> get_system() const;

private:
    Ref<const ContinuousDistribution> m_dist;
    Ref<const PolynomialSystem> m_sys;
    int m_id;
    static int s_id;
};

class GPCGerm {
public:
    void add_pair( GPCPair pair );
private:
    vector<GPCPair> m_pairs;
};

class GPCRegistry {
public:
    void add_pair( char c, const GPCPair& pair );
    GPCPair get_pair( char c ) const;

    GPCGerm create_germ( const string& str ) const;
    static GPCRegistry get_default();
private:
    map<char, GPCPair> m_char_to_gpcpair;
    static GPCRegistry s_default_reg;
};

class GPCBasis {
public:
private:
    Ref<GPCGerm> m_germ;
    Ref<MultiindexSet> m_I;
};

class GPC {
public:
private:
    Ref<GPCBasis> m_basis;
    ArrayXd m_coeffs;
};

} // namespace Alea

#endif // ALEA_GPC_GPC_HPP
