/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file common_math.hpp
 * @brief Header file for common math functions and constants
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 *
 * Most of the functions here will be gathered from existing libraries (e.g. gsl, boost, std )
 * and fitting them to the style used in Alea.
 */

#ifndef ALEA_COMMON_MATH_HPP
#define ALEA_COMMON_MATH_HPP

#include <cmath>
#define PI 3.141592653589793238462643383279502884L

#endif // ALEA_COMMON_MATH_HPP
