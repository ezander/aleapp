/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file beta_distribution.hpp
 * @brief Header file for the BetaDistribution class
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_STATISTICS_BETA_DISTRIBUTION_HPP
#define ALEA_STATISTICS_BETA_DISTRIBUTION_HPP

#include "distribution.hpp"

namespace Alea {

class BetaDistribution : public ContinuousDistribution {
public:
    BetaDistribution( double a, double b );
    ~BetaDistribution();

    virtual double mean() const;
    virtual double var() const;
    virtual double skewness() const;
    virtual double kurtosis_excess() const;

    virtual double pdf( double x ) const;
    virtual double cdf( double x ) const;
    virtual double invcdf( double y ) const;
    using ContinuousDistribution::pdf;
    using ContinuousDistribution::cdf;
    using ContinuousDistribution::invcdf;
private:
    struct impl;
    scoped_ptr<impl> m_dist;
};

} // namespace Alea

#endif // ALEA_STATISTICS_BETA_DISTRIBUTION_HPP
