/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file multiindex.hpp
 * @brief Header file for Multiindex classes and methods
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_MATHUTIL_MULTIINDEX_HPP
#define ALEA_MATHUTIL_MULTIINDEX_HPP

namespace Alea {
typedef ArrayXi MultiindexArray;

class MultiindexSet {
public:
    // Constructor
    MultiindexSet();

    // Named constructors
    static MultiindexSet create_complete_set( int m, int p );
    static MultiindexSet create_tensor_set( int m, int p );
    static MultiindexSet create_tensor_set( int m, Array1i p );

    // Getter and setter
    MultiindexArray& get_values();
    const MultiindexArray& get_values() const;
    void set_values( const MultiindexArray& new_values );

private:
    MultiindexArray values;
};

} // namespace Alea

#endif // ALEA_MATHUTIL_MULTIINDEX_HPP
