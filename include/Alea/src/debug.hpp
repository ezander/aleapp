/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file debug.hpp
 * @brief Header file for macros and functions that aid in debugging
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#ifndef ALEA_DEBUG_HPP
#define ALEA_DEBUG_HPP

#define ALEA_NOOP do {} while( 0 )

#ifndef NDEBUG
#define ALEA_DEBUG_SHOW( _symbol_ ) std::cout << # _symbol_ << ": " \
                                              << ( _symbol_ ) << std::endl
#define ALEA_DEBUG_SHOWLN( _symbol_ ) std::cout << # _symbol_ << ": \n" \
                                              << ( _symbol_ ) << std::endl
#else
#define ALEA_DEBUG_SHOW( _symbol_ ) ALEA_NOOP
#define ALEA_DEBUG_SHOWLN( _symbol_ ) ALEA_NOOP
#endif

#endif // ALEA_DEBUG_HPP
