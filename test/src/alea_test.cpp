/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file alea_tests.cpp
 * @brief Provides a main function for the unit tests
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 *
 * All that this file does is to tell Catch to provide a main() function. We do
 * that only in this file. If certain tests should be run on their own the
 * object file can just be linked against the object file of this file, or
 * better still via providing the tags on the command line of the executable.
 */

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"

#include <ios>

int main( int argc, char* const argv[] )
{
    std::ios_base::sync_with_stdio( true );

    dup2(1, 2);  //redirects stderr to stdout below this line.

    int result = Catch::Session().run( argc, argv );

    return result;
}
