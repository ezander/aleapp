/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2015 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_ref.cpp
 * @brief Unit tests for /ref.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/ref.hpp"

#include <stdexcept>

using namespace Alea;

#define BASE_TAGS "[ref]"

struct Mockup {
    int* m_counter;
    Mockup( int& counter ) : m_counter( &counter )
    {
        ( *m_counter )++;
    }
    ~Mockup()
    {
        ( *m_counter )--;
    }
};

ALEA_TEST_CASE( "Test the mockup first", BASE_TAGS "[pdf]" )
{
    int i = 0;
    {
        Mockup m( i );
        REQUIRE( i == 1 );
    }
    REQUIRE( i == 0 );
}

void empty_ref()
{
    // test seems superfluous, but this once did throw
    Ref<Mockup> r1;
    Ref<Mockup> r2;
}

ALEA_TEST_CASE( "Empty Ref",
    BASE_TAGS "[empty_ref]" )
{
    REQUIRE_NOTHROW( empty_ref() );
}

ALEA_TEST_CASE( "Now the the ref",
    BASE_TAGS "[pdf]" )
{
    int i = 0;
    {
        Ref<Mockup> r = make_ref( new Mockup( i ) );
        REQUIRE( i == 1 );
        {
            Ref<Mockup> r2 = r;
            REQUIRE( i == 1 );
        }
    }
    REQUIRE( i == 0 );
}

void may_store_in_vector( int i, vector<Ref<Mockup> >& vec, bool store )
{
    Mockup m( i );
    Ref<Mockup> r = make_ref( m );
    if( store )
        vec.push_back( r );
}

ALEA_TEST_CASE( "Now the static ref",
    BASE_TAGS "[static_ref]" )
{
    int i = 0;
    vector<Ref<Mockup> > v;
    REQUIRE_NOTHROW( may_store_in_vector( i, v, false ) );
    REQUIRE( i == 0 );

    REQUIRE_THROWS( may_store_in_vector( i, v, true ) );
}

void may_have_scoping_problem( int& i, bool problem )
{
    Ref<Mockup> r_outer;
    if( problem )
    {
        Mockup m( i );
        Ref<Mockup> r = make_ref( m );
        r_outer = r;
    }
    else
    {
        Ref<Mockup> r = make_ref( new Mockup( i ) );
        r_outer = r;
    }
}

ALEA_TEST_CASE( "Static Ref with scoping problem",
    BASE_TAGS "[static_ref]" )
{
    int i = 0;
    REQUIRE_NOTHROW( may_have_scoping_problem( i, false ) );
    REQUIRE( i == 0 );

    REQUIRE_THROWS( may_have_scoping_problem( i, true ) );
}

void may_have_ordering_problem( int& i, bool problem )
{
    if( problem )
    {
        Mockup m( i );
        Ref<Mockup> r2;
        Ref<Mockup> r = make_ref( m );
        r2 = r;
    }
    else
    {
        Mockup m( i );
        Ref<Mockup> r = make_ref( m );
        Ref<Mockup> r2;
        r2 = r;
    }
}

ALEA_TEST_CASE( "Static Ref with ordering problem",
    BASE_TAGS "[static_ref]" )
{
    int i = 0;
    REQUIRE_NOTHROW( may_have_ordering_problem( i, false ) );
    REQUIRE( i == 0 );

    REQUIRE_THROWS( may_have_ordering_problem( i, true ) );
}
