/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_polynomial_algs.cpp
 * @brief Unit tests for Bases/polynomial_algs.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/polynomial_algs.hpp"

using namespace Alea;

#define BASE_TAGS "[bases][polynomial_algs]"

ALEA_TEST_CASE( "Evaluation of polynomials", BASE_TAGS "[evaluate]" )
{
    Array1d xi( 4 );
    xi << 1, 2, 3, 4;

    Array3d rc( 4, 3 );
    rc <<
    0, 1.0 / 1.0, 0.0 / 1.0,
    0, 3.0 / 2.0, 1.0 / 2.0,
    0, 5.0 / 3.0, 2.0 / 3.0,
    0, 7.0 / 4.0, 3.0 / 4.0;

    ArrayXd expect( 5, 4 );
    expect <<
    1, 1, 1, 1,
    1, 2, 3, 4,
    1, 5.5, 13, 23.5,
    1, 17, 63, 154,
    1, 55.375, 321, 1060.375;

    ArrayXd y = poly_eval( rc, xi );

    REQUIRE( y.rows() == rc.rows() + 1 );
    REQUIRE( y.cols() == xi.size() );
    REQUIRE_THAT( y, CloseTo( expect ) );
}

ALEA_TEST_CASE( "Norm of polynomials", BASE_TAGS "[norm]" )
{
    Array3d rc( 5, 3 );
    rc <<
    0, 1.0 / 1.0, 0.0 / 1.0,
    0, 3.0 / 2.0, 1.0 / 2.0,
    0, 5.0 / 3.0, 2.0 / 3.0,
    0, 7.0 / 4.0, 3.0 / 4.0,
    0, 9.0 / 5.0, 4.0 / 5.0;

    Array1d expect( 5 );
    expect << 1 / 1.0, 1 / 3.0, 1 / 5.0, 1 / 7.0, 1 / 9.0;

    Array1d h = poly_square_norm( rc );

    REQUIRE( h.rows() == rc.rows() );
    REQUIRE_THAT( h, CloseTo( expect ) );

    rc <<
    0, 1, 0,
    0, 1, 1,
    0, 1, 2,
    0, 1, 3,
    0, 1, 4;

    expect << 1, 1, 2, 6, 24;

    h = poly_square_norm( rc );

    REQUIRE( h.rows() == rc.rows() );
    REQUIRE_THAT( h, CloseTo( expect ) );
}

ALEA_TEST_CASE( "Normalising of polynomials", BASE_TAGS "[norm]" )
{
    Array3d rc( 6, 3 );
    rc << 0, 1, 0,
    0, 1, 1,
    0, 1, 2,
    0, 1, 3,
    0, 1, 4,
    0, 1, 5;

    Array3d expect( 5, 3 );
    expect <<
    0,                 1,                 0,
    0, 0.707106781186547, 0.707106781186547,
    0, 0.577350269189626, 0.816496580927726,
    0,               0.5, 0.866025403784439,
    0, 0.447213595499958, 0.894427190999916;

    Array3d rc_normed = to_normalised_rc( rc );
    REQUIRE_THAT( rc_normed, CloseTo( expect ) );
    // The following is a requirement that holds for all normalised polynomials
    //  (in matlab: 1./b(1:end-1) - c(2:end)./b(2:end))
    REQUIRE_THAT( rc_normed.col( 1 ).segment( 1,
            4 ),
        CloseTo( Array1d( rc_normed.col( 2 ).segment( 1,
                    4 ) * rc_normed.col( 1 ).segment( 0, 4 ) ) ) );

    rc <<
    0,  1.0 / 1.0, 0.0 / 1.0,
    0,  3.0 / 2.0, 1.0 / 2.0,
    0,  5.0 / 3.0, 2.0 / 3.0,
    0,  7.0 / 4.0, 3.0 / 4.0,
    0,  9.0 / 5.0, 4.0 / 5.0,
    0, 11.0 / 6.0, 5.0 / 6.0;

    expect <<
    0, 1.73205080756888,                0,
    0, 1.93649167310371, 1.11803398874989,
    0, 1.97202659436654, 1.01835015443463,
    0, 1.98431348329844, 1.00623058987491,
    0, 1.98997487421324, 1.00285307284481;

    rc_normed = to_normalised_rc( rc );
    REQUIRE_THAT( rc_normed, CloseTo( expect ) );

    // The following is a requirement that holds for all normalised polynomials
    //  (in matlab: 1./b(1:end-1) - c(2:end)./b(2:end))
    REQUIRE_THAT( rc_normed.col( 1 ).segment( 1,
            4 ),
        CloseTo( Array1d( rc_normed.col( 2 ).segment( 1,
                    4 ) * rc_normed.col( 1 ).segment( 0, 4 ) ) ) );
}

ALEA_TEST_CASE( "Monic polynomials (rc)", BASE_TAGS "[monic]" )
{
    INFO( "Hermite RCs" );
    Array3d rc( 6, 3 );
    rc <<
    0, 1, 0,
    0, 1, 1,
    0, 1, 2,
    0, 1, 3,
    0, 1, 4,
    0, 1, 5;

    Array3d expect( 5, 3 );
    expect = rc.block( 0, 0, 5, 3 );

    Array3d rc_monic = to_monic_rc( rc );
    REQUIRE_THAT( rc_monic, CloseTo( expect ) );

    INFO( "Legendre RCs" );
    rc <<
    0,  1.0 / 1.0, 0.0 / 1.0,
    0,  3.0 / 2.0, 1.0 / 2.0,
    0,  5.0 / 3.0, 2.0 / 3.0,
    0,  7.0 / 4.0, 3.0 / 4.0,
    0,  9.0 / 5.0, 4.0 / 5.0,
    0, 11.0 / 6.0, 5.0 / 6.0;

    expect <<
    0, 1,    0,
    0, 1,  1.0 / 3.0,
    0, 1,  4.0 / 15.0,
    0, 1,  9.0 / 35.0,
    0, 1, 16.0 / 63.0;

    rc_monic = to_monic_rc( rc );
    REQUIRE_THAT( rc_monic, CloseTo( expect ) );

    INFO( "Laguerre RCs" );
    rc <<
    1.0,   -1.0,   0.0,
    3.0 / 2, -1.0 / 2, 1.0 / 2,
    5.0 / 3, -1.0 / 3, 2.0 / 3,
    7.0 / 4, -1.0 / 4, 3.0 / 4,
    9.0 / 5, -1.0 / 5, 4.0 / 5,
    11.0 / 6, -1.0 / 6, 5.0 / 6;

    expect <<
    -1, 1,  0,
    -3, 1,  1,
    -5, 1,  4,
    -7, 1,  9,
    -9, 1, 16;

    rc_monic = to_monic_rc( rc );
    REQUIRE_THAT( rc_monic, CloseTo( expect ) );
}

ALEA_TEST_CASE( "Gauss quad from rc", BASE_TAGS "[gauss][rc]" )
{
    INFO( "Hermite RCs" );
    Array3d rc( 5, 3 );
    rc <<
    0, 1, 0,
    0, 1, 1,
    0, 1, 2,
    0, 1, 3,
    0, 1, 4;

    Array1d x( 4 ), w( 4 );
    x <<  -2.33441421833898,   -0.741963784302726, 0.741963784302726,
    2.33441421833898;
    w <<   0.0458758547680685,  0.454124145231931, 0.454124145231931,
    0.0458758547680685;

    Quad1d q = gauss_rule_from_rc( rc );
    REQUIRE_THAT( q.x, CloseTo( x ) );
    REQUIRE_THAT( q.w, CloseTo( w ) );

    INFO( "Legendre RCs" );
    rc <<
    0,  1.0 / 1.0, 0.0 / 1.0,
    0,  3.0 / 2.0, 1.0 / 2.0,
    0,  5.0 / 3.0, 2.0 / 3.0,
    0,  7.0 / 4.0, 3.0 / 4.0,
    0,  9.0 / 5.0, 4.0 / 5.0;

    x << -0.861136311594052, -0.339981043584856, 0.339981043584856,
    0.861136311594052;
    w <<  0.173927422568727,  0.326072577431273, 0.326072577431273,
    0.173927422568727;

    q = gauss_rule_from_rc( rc );
    REQUIRE_THAT( q.x, CloseTo( x ) );
    REQUIRE_THAT( q.w, CloseTo( w ) );

    INFO( "Laguerre RCs" );
    rc << 1.0,   -1.0,   0.0,
    3.0 / 2, -1.0 / 2, 1.0 / 2,
    5.0 / 3, -1.0 / 3, 2.0 / 3,
    7.0 / 4, -1.0 / 4, 3.0 / 4,
    9.0 / 5, -1.0 / 5, 4.0 / 5;
    // http://www.efunda.com/math/num_integration/findgausslaguerre.cfm
    x << 0.322547689619392, 1.74576110115835, 4.53662029692113,
    9.39507091230113;
    w << 0.603154104341634, 0.3574186924378,  0.0388879085150054,
    0.000539294705561327;

    q = gauss_rule_from_rc( rc );
    REQUIRE_THAT( q.x, CloseTo( x ) );
    REQUIRE_THAT( q.w, CloseTo( w ) );
}
