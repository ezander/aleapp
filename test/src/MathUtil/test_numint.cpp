/*
 * This file is part of Alea++, a C++ library for Uncertainty Quantification.
 *
 * Copyright (C) 2014 Elmar Zander <e.zander@tu-braunschweig.de>
 *
 * This file is licensed under the terms of the GNU General Public License
 * (GPL), Version 3.0 or later. (See accompanying file COPYING.GPL or obtain
 * a copy at http://www.gnu.org/licenses/gpl.html)
 */

/**
 * @file test_numint.cpp
 * @brief Unit tests for MathUtil/numint.hpp
 * @author Elmar Zander, Inst. of Scientific Computing, TU Braunschweig
 */

#include "alea_unittest.hpp"

#include <Alea/Common>
#include <Alea/Types>
#include "Alea/src/MathUtil/numint.hpp"

using namespace Alea;

#define BASE_TAGS "[mathutil][numint]"

class demo_func
    : public UnaryFunc {
public:
    demo_func( double alpha )
        : m_alpha( alpha ) {}

    virtual double eval( double x )
    {
        return log( m_alpha * x ) / sqrt( x );
    }
private:
    double m_alpha;
};

ALEA_TEST_CASE( "Quadature1d [a,b]", BASE_TAGS "[quadrature][quad1d][finite]" )
{
    Quadrature1d Q;
    demo_func df( 1.0 );

    double result = Q.quad1d( df, 0, 1 );
    REQUIRE( result == Approx( -4.0 ) );

    double error = Q.get_last_error();
    int status = Q.get_last_status();
}
